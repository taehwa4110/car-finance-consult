package com.jth.carfinanceconsult;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarFinanceConsultApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarFinanceConsultApplication.class, args);
	}

}
