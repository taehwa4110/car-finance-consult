package com.jth.carfinanceconsult.advice;

import com.jth.carfinanceconsult.enums.ResultCode;
import com.jth.carfinanceconsult.exception.CMissingDataException;
import com.jth.carfinanceconsult.exception.CNoManufacturerDataException;
import com.jth.carfinanceconsult.model.CommonResult;
import com.jth.carfinanceconsult.service.ResponseService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

@RestControllerAdvice
public class ExceptionAdvice {
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult defaultException(HttpServletRequest request, Exception e) {
        return ResponseService.getFailResult(ResultCode.FAILED);
    }

    @ExceptionHandler(CMissingDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CMissingDataException e) {
        return ResponseService.getFailResult(ResultCode.MISSING_DATA);
    }

    @ExceptionHandler(CNoManufacturerDataException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    protected CommonResult customException(HttpServletRequest request, CNoManufacturerDataException e) {
        return ResponseService.getFailResult(ResultCode.NO_MANUFACTURER_DATA);
    }


}
