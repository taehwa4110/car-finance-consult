package com.jth.carfinanceconsult.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
