package com.jth.carfinanceconsult.exception;

public class CNoManufacturerDataException extends RuntimeException{
    public CNoManufacturerDataException(String msg, Throwable t) {super(msg, t);}

    public CNoManufacturerDataException(String msg) {
        super(msg);
    }

    public CNoManufacturerDataException() {
        super();
    }
}
