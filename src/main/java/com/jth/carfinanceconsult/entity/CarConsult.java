package com.jth.carfinanceconsult.entity;

import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import com.jth.carfinanceconsult.model.carconsult.CarConsultRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarConsult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carOptionId", nullable = false)
    private CarOption carOption;

    @Column(nullable = false, length = 20)
    private String customerName;

    @Column(nullable = false, length = 20)
    private String customerPhone;

    @Column(nullable = false)
    private LocalDateTime dateConsult;

    private CarConsult(CarConsultBuilder builder) {
        this.carOption = builder.carOption;
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.dateConsult = builder.dateConsult;
    }

    public static class CarConsultBuilder implements CommonModelBuilder<CarConsult> {
        private final CarOption carOption;
        private final String customerName;
        private final String customerPhone;
        private final LocalDateTime dateConsult;

        public CarConsultBuilder(CarOption carOption, CarConsultRequest request) {
            this.carOption = carOption;
            this.customerName = request.getCustomerName();
            this.customerPhone = request.getCustomerPhone();
            this.dateConsult = LocalDateTime.now();
        }

        @Override
        public CarConsult build() {
            return new CarConsult(this);
        }
    }
}
