package com.jth.carfinanceconsult.entity;

import com.jth.carfinanceconsult.enums.CarDriveMethod;
import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import com.jth.carfinanceconsult.model.caroption.CarOptionChangeUpdateRequest;
import com.jth.carfinanceconsult.model.caroption.CarOptionRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarOption {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carGradeId", nullable = false)
    private CarGrade carGrade;

    @Column(nullable = false, length = 20)
    private String carOptionName;

    @Column(nullable = false, length = 20)
    private String carGearBox;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private CarDriveMethod carDriveMethod;

    @Column(nullable = false)
    private Integer carHighOutput;

    @Column(nullable = false)
    private Double carMaximumTorque;

    @Column(nullable = false)
    private Double carCompoundFuelEconomy;

    @Column(nullable = false)
    private Double carCityFuelEconomy;

    @Column(nullable = false)
    private Double carHighwayFuelEconomy;

    @Column(nullable = false)
    private Integer carFuelGrade;

    @Column(nullable = false)
    private Integer carLowPollutionGrade;

    @Column(nullable = false)
    private Double carCurbWeight;

    @Column(nullable = false)
    private Integer carPersonnel;

    @Column(nullable = false)
    private Double carPrice;

    @Column(nullable = false)
    private LocalDateTime dateCreate;
    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putCarOptionChange(CarOptionChangeUpdateRequest request) {
        this.carOptionName = request.getCarOptionName();
        this.carGearBox = request.getCarGearBox();
        this.carDriveMethod = request.getCarDriveMethod();
        this.carHighOutput = request.getCarHighOutput();
        this.carMaximumTorque = request.getCarMaximumTorque();
        this.carCompoundFuelEconomy = request.getCarCompoundFuelEconomy();
        this.carCityFuelEconomy = request.getCarCityFuelEconomy();
        this.carHighwayFuelEconomy = request.getCarHighwayFuelEconomy();
        this.carFuelGrade = request.getCarFuelGrade();
        this.carLowPollutionGrade = request.getCarLowPollutionGrade();
        this.carCurbWeight = request.getCarCurbWeight();
        this.carPersonnel = request.getCarPersonnel();
        this.dateUpdate = LocalDateTime.now();
    }

    private CarOption(CarOptionBuilder builder) {
        this.carGrade = builder.carGrade;
        this.carOptionName = builder.carOptionName;
        this.carGearBox = builder.carGearBox;
        this.carDriveMethod = builder.carDriveMethod;
        this.carHighOutput = builder.carHighOutput;
        this.carMaximumTorque = builder.carMaximumTorque;
        this.carCompoundFuelEconomy = builder.carCompoundFuelEconomy;
        this.carCityFuelEconomy = builder.carCityFuelEconomy;
        this.carHighwayFuelEconomy = builder.carHighwayFuelEconomy;
        this.carFuelGrade = builder.carFuelGrade;
        this.carLowPollutionGrade = builder.carLowPollutionGrade;
        this.carCurbWeight = builder.carCurbWeight;
        this.carPersonnel = builder.carPersonnel;
        this.carPrice = builder.carPrice;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class CarOptionBuilder implements CommonModelBuilder<CarOption> {
        private final CarGrade carGrade;
        private final String carOptionName;
        private final String carGearBox;
        private final CarDriveMethod carDriveMethod;
        private final Integer carHighOutput;
        private final Double carMaximumTorque;
        private final Double carCompoundFuelEconomy;
        private final Double carCityFuelEconomy;
        private final Double carHighwayFuelEconomy;
        private final Integer carFuelGrade;
        private final Integer carLowPollutionGrade;
        private final Double carCurbWeight;
        private final Integer carPersonnel;
        private final Double carPrice;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarOptionBuilder(CarGrade carGrade, CarOptionRequest request) {
            this.carGrade = carGrade;
            this.carOptionName = request.getCarOptionName();
            this.carGearBox = request.getCarGearBox();
            this.carDriveMethod = request.getCarDriveMethod();
            this.carHighOutput = request.getCarHighOutput();
            this.carMaximumTorque = request.getCarMaximumTorque();
            this.carCompoundFuelEconomy = request.getCarCompoundFuelEconomy();
            this.carCityFuelEconomy = request.getCarCityFuelEconomy();
            this.carHighwayFuelEconomy = request.getCarHighwayFuelEconomy();
            this.carFuelGrade = request.getCarFuelGrade();
            this.carLowPollutionGrade = request.getCarLowPollutionGrade();
            this.carCurbWeight = request.getCarCurbWeight();
            this.carPersonnel = request.getCarPersonnel();
            this.carPrice = request.getCarPrice();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarOption build() {
            return new CarOption(this);
        }
    }
}
