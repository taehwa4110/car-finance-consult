package com.jth.carfinanceconsult.entity;

import com.jth.carfinanceconsult.enums.CarFuel;
import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import com.jth.carfinanceconsult.model.cargrade.CarGradeFuelUpdateRequest;
import com.jth.carfinanceconsult.model.cargrade.CarGradeRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarGrade {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carModelId", nullable = false)
    private CarModel carModel;

    @Column(nullable = false)
    private Integer carDateRelease;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private CarFuel carFuel;

    @Column(nullable = false)
    private Integer carDisplacement;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putCarGradeFuel(CarGradeFuelUpdateRequest request) {
        this.carFuel = request.getCarFuel();
        this.dateUpdate = LocalDateTime.now();
    }

    private CarGrade(CarGradeBuilder builder) {
        this.carModel = builder.carModel;
        this.carDateRelease = builder.carDateRelease;
        this.carFuel = builder.carFuel;
        this.carDisplacement = builder.carDisplacement;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class CarGradeBuilder implements CommonModelBuilder<CarGrade> {
        private final CarModel carModel;
        private final Integer carDateRelease;
        private final CarFuel carFuel;
        private final Integer carDisplacement;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarGradeBuilder(CarModel carModel, CarGradeRequest request) {
            this.carModel = carModel;
            this.carDateRelease = request.getCarDateRelease();
            this.carFuel = request.getCarFuel();
            this.carDisplacement = request.getCarDisplacement();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarGrade build() {
            return new CarGrade(this);
        }
    }
}
