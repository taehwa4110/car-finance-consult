package com.jth.carfinanceconsult.entity;

import com.jth.carfinanceconsult.enums.ManufacturerName;
import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import com.jth.carfinanceconsult.model.manufacturer.CarManufacturerJoinRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarManufacturer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private ManufacturerName manufacturerName;

    @Column(nullable = false, length = 100)
    private String manufacturerMarkImage;

    @Column(nullable = false, length = 20)
    private String manufacturerPhone;

    @Column(nullable = false)
    private Boolean isOperate;

    @Column(nullable = false)
    private LocalDateTime dateOpen;

    private LocalDateTime dateClose;

    public void putIsOperate(){
        this.isOperate = false;
        this.dateClose = LocalDateTime.now();
    }

    private CarManufacturer(CarManufacturerBuilder builder) {
        this.manufacturerName = builder.manufacturerName;
        this.manufacturerMarkImage = builder.manufacturerMarkImage;
        this.manufacturerPhone = builder.manufacturerPhone;
        this.isOperate = builder.isOperate;
        this.dateOpen = builder.dateOpen;
    }
    public static class CarManufacturerBuilder implements CommonModelBuilder<CarManufacturer> {
        private final ManufacturerName manufacturerName;
        private final String manufacturerMarkImage;
        private final String manufacturerPhone;
        private final Boolean isOperate;
        private final LocalDateTime dateOpen;

        public CarManufacturerBuilder(CarManufacturerJoinRequest request) {
            this.manufacturerName = request.getManufacturerName();
            this.manufacturerMarkImage = request.getManufacturerMarkImage();
            this.manufacturerPhone = request.getManufacturerPhone();
            this.isOperate = true;
            this.dateOpen = LocalDateTime.now();
        }

        @Override
        public CarManufacturer build() {
            return new CarManufacturer(this);
        }
    }
}
