package com.jth.carfinanceconsult.entity;

import com.jth.carfinanceconsult.enums.CarExternal;
import com.jth.carfinanceconsult.enums.CarReleaseSituation;
import com.jth.carfinanceconsult.enums.CarType;
import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import com.jth.carfinanceconsult.model.carmodel.CarModelBuyRequest;
import com.jth.carfinanceconsult.model.carmodel.CarModelTypeUpdateRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carManufacturerId", nullable = false)
    private CarManufacturer carManufacturer;

    @Column(nullable = false, length = 100)
    private String carImageUrl;

    @Column(nullable = false, length = 30)
    private String carName;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private CarType carType;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private CarExternal carExternal;

    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private CarReleaseSituation carReleaseSituation;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putCarType(CarModelTypeUpdateRequest request) {
        this.carType = request.getCarType();
        this.dateUpdate = LocalDateTime.now();
    }



    private CarModel(CarModelBuilder builder) {
        this.carManufacturer = builder.carManufacturer;
        this.carName = builder.carName;
        this.carImageUrl = builder.carImageUrl;
        this.carType = builder.carType;
        this.carExternal = builder.carExternal;
        this.carReleaseSituation = builder.carReleaseSituation;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }
    public static class CarModelBuilder implements CommonModelBuilder<CarModel> {
        private final CarManufacturer carManufacturer;
        private final String carName;

        private final String carImageUrl;
        private final CarType carType;
        private final CarExternal carExternal;
        private final CarReleaseSituation carReleaseSituation;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public CarModelBuilder(CarManufacturer carManufacturer,CarModelBuyRequest request) {
            this.carManufacturer = carManufacturer;
            this.carName = request.getCarName();
            this.carImageUrl = request.getCarImageUrl();
            this.carType = request.getCarType();
            this.carExternal = request.getCarExternal();
            this.carReleaseSituation = request.getCarReleaseSituation();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public CarModel build() {
            return new CarModel(this);
        }
    }
}
