package com.jth.carfinanceconsult.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarExternal {
    SEDAN("세단"),
    COUPE("쿠페"),
    SUV("SUV"),
    VAN("밴"),
    TRUCK("트럭"),
    HATCHBACK("해치백"),
    WAGON("왜건"),
    MINIBUS("미니버스");

    private final String name;
}
