package com.jth.carfinanceconsult.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarType {
    LIGHTCAR("경차"),
    SMALLTYPE("소형"),
    MEDIUMTYPE("중형"),
    LARGETYPE("대형");

    private final String name;
}
