package com.jth.carfinanceconsult.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarDriveMethod {
    FF("앞바퀴 구동"),
    FR("뒷바퀴 구동"),
    FOURWD("사륜 구동");

    private final String name;
}
