package com.jth.carfinanceconsult.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ManufacturerName {
    KIA("기아"),
    HYUNDAI("현대"),
    SSANGYONG("쌍용"),
    RENAULTKOREA("르노 코리아"),
    CHEVROLET("쉐보레"),
    GENESIS("제네시스"),
    BMW("BMW"),
    BENZ("벤츠"),
    AUDI("아우디"),
    VOLKSWAGEN("폭스바겐");

    private final String name;
}
