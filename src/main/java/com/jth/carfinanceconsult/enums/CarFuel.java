package com.jth.carfinanceconsult.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarFuel {
    LPG("LPG"),
    DIESEL("디젤"),
    GASOLINE("가솔린"),
    HYBRID("하이브리드"),
    ELECTRIC("전기");

    private final String name;
}
