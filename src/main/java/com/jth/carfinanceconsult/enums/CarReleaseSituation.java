package com.jth.carfinanceconsult.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CarReleaseSituation {
    ON_SALE("시판"),
    EXPECT("예정"),
    DISCONTINUANCE("단종");

    private final String name;
}
