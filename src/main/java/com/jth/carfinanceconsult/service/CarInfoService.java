package com.jth.carfinanceconsult.service;

import com.jth.carfinanceconsult.entity.CarGrade;
import com.jth.carfinanceconsult.entity.CarManufacturer;
import com.jth.carfinanceconsult.entity.CarModel;
import com.jth.carfinanceconsult.entity.CarOption;
import com.jth.carfinanceconsult.exception.CMissingDataException;
import com.jth.carfinanceconsult.exception.CNoManufacturerDataException;
import com.jth.carfinanceconsult.model.CarInfoDetailResponse;
import com.jth.carfinanceconsult.model.CarListItem;
import com.jth.carfinanceconsult.model.ListResult;
import com.jth.carfinanceconsult.model.cargrade.CarGradeFuelUpdateRequest;
import com.jth.carfinanceconsult.model.cargrade.CarGradeRequest;
import com.jth.carfinanceconsult.model.carmodel.CarModelBuyRequest;
import com.jth.carfinanceconsult.model.carmodel.CarModelTypeUpdateRequest;
import com.jth.carfinanceconsult.model.caroption.CarOptionChangeUpdateRequest;
import com.jth.carfinanceconsult.model.caroption.CarOptionRequest;
import com.jth.carfinanceconsult.model.manufacturer.CarManufacturerJoinRequest;
import com.jth.carfinanceconsult.repository.CarGradeRepository;
import com.jth.carfinanceconsult.repository.CarManufacturerRepository;
import com.jth.carfinanceconsult.repository.CarModelRepository;
import com.jth.carfinanceconsult.repository.CarOptionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;


@Service
@RequiredArgsConstructor
public class CarInfoService {
    private final CarManufacturerRepository carManufacturerRepository;
    private final CarModelRepository carModelRepository;
    private final CarGradeRepository carGradeRepository;
    private final CarOptionRepository carOptionRepository;

    /**
     * 차량 제조사 등록
     * @param joinRequest 차량 제조사를 등록하기 위해 필요로 하는 정보
     */
    public void setCarManufacturer(CarManufacturerJoinRequest joinRequest) {
        CarManufacturer carManufacturer = new CarManufacturer.CarManufacturerBuilder(joinRequest).build();
        carManufacturerRepository.save(carManufacturer);
    }

    /**
     * 차량 제조사 정보 주기
     * @param carManufacturerId 차량 제조사 시퀀스
     * @return 차량 제조사 정보
     */
    public CarManufacturer getCarManufacturerData(long carManufacturerId) {
        return carManufacturerRepository.findById(carManufacturerId).orElseThrow(CMissingDataException::new);
    }

    /**
     * 차량 제조사 운영 여부 수정하기
     * @param carManufactureId 차량 제조사 시퀀스
     */
    public void putCarManufacturerClose(long carManufactureId) {
        CarManufacturer carManufacturer = carManufacturerRepository.findById(carManufactureId).orElseThrow(CMissingDataException::new);

        if (!carManufacturer.getIsOperate())throw new CNoManufacturerDataException();

        carManufacturer.putIsOperate();
        carManufacturerRepository.save(carManufacturer);
    }

    /**
     * 차량 모델 등록하기
     * @param carManufacturer 차량 제조사 정보
     * @param request 차량 모델를 등록하기 위해 필요로 하는 데이터
     */
    public void setCarModel(CarManufacturer carManufacturer, CarModelBuyRequest request) {
        CarModel carModel = new CarModel.CarModelBuilder(carManufacturer, request).build();
        carModelRepository.save(carModel);
    }

    /**
     * 차량 모델 정보 주기
     * @param carModelId 차량 모델 시퀀스
     * @return 차량 모델
     */
    public CarModel getCarModelData(long carModelId) {
        return carModelRepository.findById(carModelId).orElseThrow(CMissingDataException::new);
    }

    /**
     * 차량 타입 수정
     * @param carModelId 차량 모델 시퀀스
     * @param request 차량 타입 수정을 위해 필요한 데이터
     */
    public void putCarModelType(long carModelId, CarModelTypeUpdateRequest request) {
        CarModel carModel = carModelRepository.findById(carModelId).orElseThrow(CMissingDataException::new);
        carModel.putCarType(request);

        carModelRepository.save(carModel);
    }

    /**
     * 차량 등급 등록하기
     * @param carModel 차량 모델 정보
     * @param request 차량 등급을 등록하기 위해 필요로 하는 값
     */
    public void setCarGrade(CarModel carModel, CarGradeRequest request) {
        CarGrade carGrade = new CarGrade.CarGradeBuilder(carModel, request).build();
        carGradeRepository.save(carGrade);
    }

    /**
     * 차량 등급 정보 주기
     * @param carGradeId 차량 등급 시퀀스
     * @return 차량 등급 정보
     */
    public CarGrade getCarGradeData(long carGradeId) {
        return carGradeRepository.findById(carGradeId).orElseThrow(CMissingDataException::new);
    }

    /**
     * 차량 연료 정보 수정
     * @param carGradeId 차량 등급 시퀀스
     * @param request 차량 연료 정보를 수정하기 위해 필요로 하는 데이터
     */
    public void putCarGradeFuel(long carGradeId, CarGradeFuelUpdateRequest request) {
        CarGrade carGrade = carGradeRepository.findById(carGradeId).orElseThrow(CMissingDataException::new);
        carGrade.putCarGradeFuel(request);

        carGradeRepository.save(carGrade);
    }

    /**
     * 차량 옵션 등록하기
     * @param carGrade 차량 등급 정보
     * @param request 차량 옵션 정보를 등록하기 위해 필요로 하는 데이터
     */
    public void setCarOption(CarGrade carGrade, CarOptionRequest request) {
        CarOption carOption = new CarOption.CarOptionBuilder(carGrade, request).build();
        carOptionRepository.save(carOption);
    }

    /**
     * 차량 옵션 정보 주기
     * @param carOptionId 차량 옵션 시퀀스
     * @return 차량 옵션 정보
     */
    public CarOption getCarOptionData(long carOptionId) {
        return carOptionRepository.findById(carOptionId).orElseThrow(CMissingDataException::new);
    }

    /**
     * 차량 옵션 정보 수정하기
     * @param carOptionId 차량 옵션 시퀀스
     * @param request 차량 옵션 정보 수정하기 위해 필요로 하는 데이터
     */
    public void putCarOption(long carOptionId, CarOptionChangeUpdateRequest request) {
        CarOption carOption = carOptionRepository.findById(carOptionId).orElseThrow();
        carOption.putCarOptionChange(request);

        carOptionRepository.save(carOption);
    }

    /**
     * 차량 리스트 가져오기
     * @return 차량 리스트
     */
    public ListResult<CarListItem> getCarModels() {
        List<CarListItem> result = new LinkedList<>();

        List<CarModel> carModels = carModelRepository.findAll();

        carModels.forEach(carModel -> {
            CarListItem carListItem = new CarListItem.CarListItemBuilder(carModel).build();
            result.add(carListItem);
        });

        return ListConvertService.settingResult(result);
    }

    /**
     * 차량 상세정보 가져오기
     * @param carModelId 차량 모델 시퀀스
     * @return 차량 상세정보
     */
    public CarInfoDetailResponse getCarInfoDetail(long carModelId) {
        CarModel carModel = carModelRepository.findById(carModelId).orElseThrow(CMissingDataException::new);
        List<CarOption> carOptions = carOptionRepository.findAllByCarGrade_CarModel_Id(carModel.getId());

        ArrayList<Double> prices = new ArrayList<>(); //Double 형태의 임시 가격 배열생성
        carOptions.forEach(carOption -> {
            prices.add(carOption.getCarPrice()); //CarOption 리스트에서 금액들만 뽑아서 가격 배열에다가 넣는다.
        });

        Collections.sort(prices); //배열을 오름차순으로 정렬시키고 있습니다.

        Double minPrice = prices.get(0); //가장 최소값이 인덱스 맨앞에 있습니다.
        Double maxPrice = prices.get(prices.size() - 1); //가장 큰 값은 인덱스의 사이즈에서 -1을 해준다.

        List<String> carFuelTypes = new LinkedList<>(); // String 형태의 차 연료 리스트 생성

        carOptions.forEach(carOption -> {
            carFuelTypes.add(carOption.getCarGrade().getCarFuel().getName()); //carOption 리스트에서 연료들만 뽑아서 연료 리스트에다가 넣는다.
        });

        Set<String> tempSet = new HashSet<>(carFuelTypes); // 중복을 허용하지 않는 Set에 넣어 중복되는 값들을 없애준다.
        List<String> carFuelTypeResult = new LinkedList<>(tempSet); //근데 빌더에서 원하는건 List형태이므로 set을 List형태로 다시 변환을 해준다.


        ArrayList<Integer> displacement = new ArrayList<>(); //Integer 형태의 임시 배기량 배열 생성
        carOptions.forEach(carOption -> {
            displacement.add(carOption.getCarGrade().getCarDisplacement()); //carOption 리스트에서 배기량들만 뽑아서 배기량 배열에다가 넣는다.
        });

        Collections.sort(displacement); //배열을 오름차순으로 정렬시키고 있습니다.

        Integer minDisplacement = displacement.get(0); //가장 최소값이 인덱스 맨앞에 있습니다.
        Integer maxDisplacement = displacement.get(displacement.size() - 1); //가장 큰 값은 인덱스의 사이즈에서 -1을 해준다.

        ArrayList<Double> carCompoundFuelEconomy = new ArrayList<>(); //Double 형태의 임시 복합연비 배열생성
        carOptions.forEach(carOption -> {
            carCompoundFuelEconomy.add(carOption.getCarCompoundFuelEconomy()); //carOption 리스트에서 복합연비들만 뽑아서 복합연비 배열에다가 넣는다.
        });

        Collections.sort(carCompoundFuelEconomy); //배열을 오름차순으로 정렬시키고 있습니다.

        Double minCarCompoundFuelEconomy = carCompoundFuelEconomy.get(0); //가장 최소값이 인덱스 맨앞에 있습니다.
        Double maxCarCompoundFuelEconomy = carCompoundFuelEconomy.get(carCompoundFuelEconomy.size() - 1); //가장 큰 값은 인덱스의 사이즈에서 -1을 해준다.

        ArrayList<Integer> carPersonnel = new ArrayList<>(); //Integer 형태의 임시 정원 배열 생성
        carOptions.forEach(carOption -> {
            carPersonnel.add(carOption.getCarPersonnel()); //carOption 리스트에서 정원들만 뽑아서 정원 배열에다가 넣는다.
        });

        Collections.sort(carPersonnel); //배열을 오름차순으로 정렬시키고 있습니다.

        Integer minPersonnel = carPersonnel.get(0); //가장 최소값이 인덱스 맨앞에 있습니다.
        Integer maxPersonnel = carPersonnel.get(carPersonnel.size() - 1); //가장 큰 값은 인덱스의 사이즈에서 -1을 해준다.

        return new CarInfoDetailResponse.CarInfoDetailResponseBuilder(
                carModel,
                minPrice,
                maxPrice,
                carFuelTypeResult,
                minDisplacement,
                maxDisplacement,
                minCarCompoundFuelEconomy,
                maxCarCompoundFuelEconomy,
                minPersonnel,
                maxPersonnel
        ).build();
    }
}
