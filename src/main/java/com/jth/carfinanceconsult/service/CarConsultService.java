package com.jth.carfinanceconsult.service;

import com.jth.carfinanceconsult.entity.CarConsult;
import com.jth.carfinanceconsult.entity.CarOption;
import com.jth.carfinanceconsult.model.ListResult;
import com.jth.carfinanceconsult.model.carconsult.CarConsultItem;
import com.jth.carfinanceconsult.model.carconsult.CarConsultRequest;
import com.jth.carfinanceconsult.repository.CarConsultRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CarConsultService {
    private final CarConsultRepository carConsultRepository;

    /**
     * 차량 금융 상담 내역 등록하기
     * @param carOption 차량 옵션
     * @param request 차량 금융 상담을 받기 위해 필요로 하는 데이터 정보
     */
    public void setCarConsult(CarOption carOption, CarConsultRequest request) {
        CarConsult carConsult = new CarConsult.CarConsultBuilder(carOption, request).build();
        carConsultRepository.save(carConsult);
    }

    /**
     * 기간내에 차량 금융  상담 내역 리스트 가져오기
     * @param dateStart 기간 시작일
     * @param dateEnd 기간 종료일
     * @return 차량 금융 상담 내역 리스트
     */
    public ListResult<CarConsultItem> getCarConsults(LocalDate dateStart, LocalDate dateEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(dateStart.getYear(),
                dateStart.getMonthValue(),
                dateStart.getDayOfMonth(),
                0,
                0,
                0
        );
        LocalDateTime dateEndTime = LocalDateTime.of(dateEnd.getYear(),
                dateEnd.getMonthValue(),
                dateEnd.getDayOfMonth(),
                23,
                59,
                59
        );
        List<CarConsult> carConsults = carConsultRepository.findCarConsultByDateConsultGreaterThanEqualAndDateConsultLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);

        List<CarConsultItem> result = new LinkedList<>();

        carConsults.forEach(carConsult -> {
            CarConsultItem carConsultItem = new CarConsultItem.CarConsultItemBuilder(carConsult).build();
            result.add(carConsultItem);
        });

        return ListConvertService.settingResult(result);
    }
}
