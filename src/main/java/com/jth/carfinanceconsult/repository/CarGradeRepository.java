package com.jth.carfinanceconsult.repository;

import com.jth.carfinanceconsult.entity.CarGrade;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarGradeRepository extends JpaRepository<CarGrade, Long> {
}
