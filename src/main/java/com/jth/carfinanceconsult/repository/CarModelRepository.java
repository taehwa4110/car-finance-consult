package com.jth.carfinanceconsult.repository;

import com.jth.carfinanceconsult.entity.CarModel;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CarModelRepository extends JpaRepository<CarModel, Long> {
}
