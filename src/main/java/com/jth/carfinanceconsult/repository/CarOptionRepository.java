package com.jth.carfinanceconsult.repository;

import com.jth.carfinanceconsult.entity.CarOption;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CarOptionRepository extends JpaRepository<CarOption, Long> {
    List<CarOption> findAllByCarGrade_CarModel_Id(long carModelId);
}
