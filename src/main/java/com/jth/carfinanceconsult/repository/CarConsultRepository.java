package com.jth.carfinanceconsult.repository;

import com.jth.carfinanceconsult.entity.CarConsult;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface CarConsultRepository extends JpaRepository<CarConsult, Long> {
    List<CarConsult> findCarConsultByDateConsultGreaterThanEqualAndDateConsultLessThanEqualOrderByIdDesc(LocalDateTime dateStart, LocalDateTime dateEnd);
}
