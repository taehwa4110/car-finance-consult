package com.jth.carfinanceconsult.repository;

import com.jth.carfinanceconsult.entity.CarManufacturer;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CarManufacturerRepository extends JpaRepository<CarManufacturer, Long> {
}
