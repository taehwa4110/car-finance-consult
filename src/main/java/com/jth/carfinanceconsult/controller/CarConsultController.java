package com.jth.carfinanceconsult.controller;

import com.jth.carfinanceconsult.entity.CarOption;
import com.jth.carfinanceconsult.model.CommonResult;
import com.jth.carfinanceconsult.model.ListResult;
import com.jth.carfinanceconsult.model.carconsult.CarConsultItem;
import com.jth.carfinanceconsult.model.carconsult.CarConsultRequest;
import com.jth.carfinanceconsult.service.CarConsultService;
import com.jth.carfinanceconsult.service.CarInfoService;
import com.jth.carfinanceconsult.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "차량 금융 상담 내역 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/car-consult")
public class CarConsultController {
    private final CarConsultService carConsultService;
    private final CarInfoService carInfoService;

    @ApiOperation(value = "차량 금융 상담 내역 정보 등록하기")
    @PostMapping("/car-consult/new")
    public CommonResult setCarConsult(@RequestBody @Valid CarConsultRequest request) {
        CarOption carOption = carInfoService.getCarOptionData(request.getCarOptionId());
        carConsultService.setCarConsult(carOption, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 금융 상담 내역 리스트 가져오기")
    @GetMapping("/search")
    public ListResult<CarConsultItem> getCarConsults(
            @RequestParam(value = "dateStart")LocalDate dateStart,
            @RequestParam(value = "dateEnd")LocalDate dateEnd) {
        return ResponseService.getListResult(carConsultService.getCarConsults(dateStart, dateEnd), true);
    }
}
