package com.jth.carfinanceconsult.controller;

import com.jth.carfinanceconsult.entity.CarGrade;
import com.jth.carfinanceconsult.entity.CarManufacturer;
import com.jth.carfinanceconsult.entity.CarModel;
import com.jth.carfinanceconsult.model.*;
import com.jth.carfinanceconsult.model.cargrade.CarGradeFuelUpdateRequest;
import com.jth.carfinanceconsult.model.cargrade.CarGradeRequest;
import com.jth.carfinanceconsult.model.carmodel.CarModelBuyRequest;
import com.jth.carfinanceconsult.model.carmodel.CarModelTypeUpdateRequest;
import com.jth.carfinanceconsult.model.caroption.CarOptionChangeUpdateRequest;
import com.jth.carfinanceconsult.model.caroption.CarOptionRequest;
import com.jth.carfinanceconsult.model.manufacturer.CarManufacturerJoinRequest;
import com.jth.carfinanceconsult.service.CarInfoService;
import com.jth.carfinanceconsult.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@Api(tags = "차량 정보 프로그램")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/car-info")
public class CarInfoController {
    private final CarInfoService carInfoService;
    @ApiOperation(value = "차량 제조사 정보 등록하기")
    @PostMapping("/car-manufacturer/new")
    public CommonResult setCarManufacturer(@RequestBody @Valid CarManufacturerJoinRequest joinRequest) {
        carInfoService.setCarManufacturer(joinRequest);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "차량 제조사 정보 삭제하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "carManufacturer", value = "차량 제조사 시퀀스", required = true)
    })
    @DeleteMapping("/car-manufacturer/{carManufacturerId}")
    public CommonResult delCarManufacturer(@PathVariable long carManufacturerId) {
        carInfoService.putCarManufacturerClose(carManufacturerId);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "차량 모델 정보 등록하기")
    @PostMapping("/car-model/new")
    public CommonResult setCarModel(@RequestBody @Valid CarModelBuyRequest request) {
        CarManufacturer carManufacturer = carInfoService.getCarManufacturerData(request.getCarManufacturerId());
        carInfoService.setCarModel(carManufacturer, request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "차량 모델 타입 정보 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "carModelId", value = "차량 모델 시퀀스", required = true)
    })
    @PutMapping("/car-model/{carModelId}")
    public CommonResult putCarModelType(@PathVariable long carModelId, @RequestBody @Valid CarModelTypeUpdateRequest request) {
        carInfoService.putCarModelType(carModelId, request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "차량 등급 정보 등록하기")
    @PostMapping("/car-grade/new")
    public CommonResult setCarGrade(@RequestBody @Valid CarGradeRequest request) {
        CarModel carModel = carInfoService.getCarModelData(request.getCarModelId());
        carInfoService.setCarGrade(carModel, request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "차량 등급 연료 정보 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "carGradeId", value = "차량 등급 시퀀스", required = true)
    })
    @PutMapping("/car-grade/{carGradeId}")
    public CommonResult putCarGradeFuel(@PathVariable long carGradeId, @RequestBody @Valid CarGradeFuelUpdateRequest request) {
        carInfoService.putCarGradeFuel(carGradeId, request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "차량 옵션 정보 등록하기")
    @PostMapping("/car-option/new")
    public CommonResult setCarOption(@RequestBody @Valid CarOptionRequest request){
        CarGrade carGrade = carInfoService.getCarGradeData(request.getCarGradeId());
        carInfoService.setCarOption(carGrade, request);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "차량 옵션 정보 수정하기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "carOptionId", value = "차량 옵션 시퀀스", required = true)
    })
    @PutMapping("/car-option/{carOptionId}")
    public CommonResult putCarOption(@PathVariable long carOptionId, @RequestBody @Valid CarOptionChangeUpdateRequest request) {
        carInfoService.putCarOption(carOptionId, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "차량 리스트 가져오기")
    @GetMapping("/list")
    public ListResult<CarListItem> getCarList() {
        return ResponseService.getListResult(carInfoService.getCarModels(), true);
    }

    @ApiOperation(value = "차량 상세정보 가져오기")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "carModelId", value = "차량 모델 시퀀스", required = true)
    })
    @GetMapping("/detail/car-model-id/{carModelId}")
    public SingleResult<CarInfoDetailResponse> getCarInfoDetail(@PathVariable long carModelId) {
        return ResponseService.getSingleResult(carInfoService.getCarInfoDetail(carModelId));
    }
}

