package com.jth.carfinanceconsult.model;

import com.jth.carfinanceconsult.entity.CarModel;
import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarInfoDetailResponse {
    @ApiModelProperty(notes = "제조사명과 차량명")
    private String carFullName;
    @ApiModelProperty(notes = "차량 가격")
    private CarInfoDetailRangeItem carPrice;
    @ApiModelProperty(notes = "차량 타입")
    private String carType;
    @ApiModelProperty(notes = "차량 연료")
    private List<String> carFuelTypeName;
    @ApiModelProperty(notes = "차량 배기량")
    private CarInfoDetailRangeItem carDisplacement;
    @ApiModelProperty(notes = "차량 복합연비")
    private CarInfoDetailRangeItem carCompoundFuelEconomy;
    @ApiModelProperty(notes = "차량 정원")
    private CarInfoDetailRangeItem carPersonnel;

    private CarInfoDetailResponse(CarInfoDetailResponseBuilder builder) {
        this.carFullName = builder.carFullName;
        this.carPrice = builder.carPrice;
        this.carType = builder.carType;
        this.carFuelTypeName = builder.carFuelTypeName;
        this.carDisplacement = builder.carDisplacement;
        this.carCompoundFuelEconomy = builder.carCompoundFuelEconomy;
        this.carPersonnel = builder.carPersonnel;
    }
    public static class CarInfoDetailResponseBuilder implements CommonModelBuilder<CarInfoDetailResponse> {
        private final String carFullName;
        private final CarInfoDetailRangeItem carPrice;
        private final String carType;
        private final List<String> carFuelTypeName;
        private final CarInfoDetailRangeItem carDisplacement;
        private final CarInfoDetailRangeItem carCompoundFuelEconomy;
        private final CarInfoDetailRangeItem carPersonnel;

        public CarInfoDetailResponseBuilder(
                CarModel carModel,
                Double minPrice,
                Double maxPrice,
                List<String> carFuelTypeName,
                Integer minDisplacement,
                Integer maxDisplacement,
                Double minCarCompoundFuelEconomy,
                Double maxCarCompoundFuelEconomy,
                Integer minCarPersonnel,
                Integer maxCarPersonnel
        ) {
            this. carFullName = "[" + carModel.getCarManufacturer().getManufacturerName().getName() + "]" + carModel.getCarName();

            CarInfoDetailRangeItem carPrice = new CarInfoDetailRangeItem();
            carPrice.setMaxDoubleValue(maxPrice);
            carPrice.setMinDoubleValue(minPrice);
            this.carPrice = carPrice;

            this.carType = carModel.getCarExternal().getName() + "(" + carModel.getCarType().getName() + ")";

            this.carFuelTypeName = carFuelTypeName;

            CarInfoDetailRangeItem carDisplacement = new CarInfoDetailRangeItem();
            carDisplacement.setMaxIntValue(maxDisplacement);
            carDisplacement.setMinIntValue(minDisplacement);
            this.carDisplacement = carDisplacement;

            CarInfoDetailRangeItem carCompoundFuelEconomy = new CarInfoDetailRangeItem();
            carCompoundFuelEconomy.setMaxDoubleValue(maxCarCompoundFuelEconomy);
            carCompoundFuelEconomy.setMinDoubleValue(minCarCompoundFuelEconomy);
            this.carCompoundFuelEconomy = carCompoundFuelEconomy;

            CarInfoDetailRangeItem carPersonnel = new CarInfoDetailRangeItem();
            carPersonnel.setMaxIntValue(maxCarPersonnel);
            carPersonnel.setMinIntValue(minCarPersonnel);
            this.carPersonnel = carPersonnel;
        }

        @Override
        public CarInfoDetailResponse build() {
            return new CarInfoDetailResponse(this);
        }
    }

}
