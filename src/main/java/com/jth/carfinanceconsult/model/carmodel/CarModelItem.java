package com.jth.carfinanceconsult.model.carmodel;

import com.jth.carfinanceconsult.entity.CarModel;
import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModelItem {
    @ApiModelProperty(notes = "차량 모델 시퀀스")
    private Long carModelId;
    @ApiModelProperty(notes = "차량 사진")
    private String carImageUrl;
    @ApiModelProperty(notes = "차량 제조사명과 차량명")
    private String carFullName;
    @ApiModelProperty(notes = "차량 모양")
    private String carShape;
    @ApiModelProperty(notes = "차량 출시상태")
    private String carReleaseSituation;
    @ApiModelProperty(notes = "차량 제조사 전화번호")
    private String carManufacturerPhone;

    private CarModelItem(CarModelItemBuilder builder) {
        this.carModelId = builder.carModelId;
        this.carImageUrl = builder.carImageUrl;
        this.carFullName = builder.carFullName;
        this.carShape = builder.carShape;
        this.carReleaseSituation = builder.carReleaseSituation;
        this.carManufacturerPhone = builder.carManufacturerPhone;
    }
    public static class CarModelItemBuilder implements CommonModelBuilder<CarModelItem> {
        private final Long carModelId;
        private final String carImageUrl;
        private final String carFullName;
        private final String carShape;
        private final String carReleaseSituation;
        private final String carManufacturerPhone;

        public CarModelItemBuilder(CarModel carModel) {
            this.carModelId = carModel.getId();
            this.carImageUrl = carModel.getCarImageUrl();
            this.carFullName = "[" + carModel.getCarManufacturer().getManufacturerName().getName() + "]" + carModel.getCarName();
            this.carShape = carModel.getCarExternal().getName()+ "(" + carModel.getCarType().getName() + ")";
            this.carReleaseSituation = carModel.getCarReleaseSituation().getName();
            this.carManufacturerPhone = carModel.getCarManufacturer().getManufacturerPhone();
        }

        @Override
        public CarModelItem build() {
            return new CarModelItem(this);
        }
    }
}
