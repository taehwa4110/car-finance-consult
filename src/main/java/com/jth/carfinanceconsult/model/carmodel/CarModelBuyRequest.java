package com.jth.carfinanceconsult.model.carmodel;

import com.jth.carfinanceconsult.enums.CarExternal;
import com.jth.carfinanceconsult.enums.CarReleaseSituation;
import com.jth.carfinanceconsult.enums.CarType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarModelBuyRequest {
    @ApiModelProperty(notes = "차량 제조사 시퀀스", required = true)
    @NotNull
    private Long carManufacturerId;

    @ApiModelProperty(notes = "차량명 (2~30자)", required = true)
    @NotNull
    @Length(min = 2, max = 30)
    private String carName;

    @ApiModelProperty(notes = "차량 이미지 (~100자)", required = true)
    @NotNull
    @Length(max = 100)
    private String carImageUrl;

    @ApiModelProperty(notes = "차량 타입", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CarType carType;

    @ApiModelProperty(notes = "차량 외장", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CarExternal carExternal;

    @ApiModelProperty(notes = "차량 출시상태", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CarReleaseSituation carReleaseSituation;
}
