package com.jth.carfinanceconsult.model.carmodel;

import com.jth.carfinanceconsult.enums.CarType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarModelTypeUpdateRequest {
    @ApiModelProperty(notes = "차량 타입", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CarType carType;
}
