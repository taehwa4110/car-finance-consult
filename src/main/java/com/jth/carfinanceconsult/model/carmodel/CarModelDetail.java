package com.jth.carfinanceconsult.model.carmodel;

import com.jth.carfinanceconsult.entity.CarModel;
import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModelDetail {
    @ApiModelProperty(notes = "차량 모델 시퀀스")
    private Long carModelId;
    @ApiModelProperty(notes = "차량 제조사명")
    private String carManufacturerName;
    @ApiModelProperty(notes = "차량명")
    private String carName;
    @ApiModelProperty(notes = "차량 이미지")
    private String carImageUrl;
    @ApiModelProperty(notes = "차량 타입")
    private String carType;
    @ApiModelProperty(notes = "차량 외장")
    private String carExternal;
    @ApiModelProperty(notes = "차량 출시상태")
    private String carReleaseSituation;
    @ApiModelProperty(notes = "차량 제조사 전화번호")
    private String carManufacturerPhone;

    private CarModelDetail(CarModelDetailBuilder builder) {
        this.carModelId = builder.carModelId;
        this.carManufacturerName = builder.carManufacturerName;
        this.carName = builder.carName;
        this.carImageUrl = builder.carImageUrl;
        this.carType = builder.carType;
        this.carExternal = builder.carExternal;
        this.carReleaseSituation = builder.carReleaseSituation;
        this.carManufacturerPhone = builder.carManufacturerPhone;

    }
    public static class CarModelDetailBuilder implements CommonModelBuilder<CarModelDetail> {
        private final Long carModelId;
        private final String carManufacturerName;
        private final String carName;
        private final String carImageUrl;
        private final String carType;
        private final String carExternal;
        private final String carReleaseSituation;
        private final String carManufacturerPhone;

        public CarModelDetailBuilder(CarModel carModel) {
            this.carModelId = carModel.getId();
            this.carManufacturerName = carModel.getCarManufacturer().getManufacturerName().getName();
            this.carName = carModel.getCarName();
            this.carImageUrl = carModel.getCarImageUrl();
            this.carType = carModel.getCarType().getName();
            this.carExternal = carModel.getCarExternal().getName();
            this.carReleaseSituation = carModel.getCarReleaseSituation().getName();
            this.carManufacturerPhone = carModel.getCarManufacturer().getManufacturerPhone();
        }

        @Override
        public CarModelDetail build() {
            return new CarModelDetail(this);
        }
    }
}
