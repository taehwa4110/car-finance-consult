package com.jth.carfinanceconsult.model.carconsult;

import com.jth.carfinanceconsult.entity.CarConsult;
import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarConsultItem {
    @ApiModelProperty(notes = "차량 금융 상담 내역 시퀀스")
    private Long carConsultId;
    @ApiModelProperty(notes = "차량 제조사명과 차량명")
    private String carFullName;
    @ApiModelProperty(notes = "고객 이름")
    private String customerName;
    @ApiModelProperty(notes = "차량 금융 상담 날짜")
    private LocalDateTime dateConsult;

    private CarConsultItem(CarConsultItemBuilder builder) {
        this.carConsultId = builder.carConsultId;
        this.carFullName = builder.carFullName;
        this.customerName = builder.customerName;
        this.dateConsult = builder.dateConsult;
    }
    public static class CarConsultItemBuilder implements CommonModelBuilder<CarConsultItem> {
        private final Long carConsultId;
        private final String carFullName;
        private final String customerName;
        private final LocalDateTime dateConsult;

        public CarConsultItemBuilder(CarConsult carConsult) {
            this.carConsultId = carConsult.getId();
            this.carFullName = "[" + carConsult.getCarOption().getCarGrade().getCarModel().getCarManufacturer().getManufacturerName().getName()+ "]" + carConsult.getCarOption().getCarGrade().getCarModel().getCarName();
            this.customerName = carConsult.getCustomerName();
            this.dateConsult = carConsult.getDateConsult();
        }

        @Override
        public CarConsultItem build() {
            return new CarConsultItem(this);
        }
    }

}
