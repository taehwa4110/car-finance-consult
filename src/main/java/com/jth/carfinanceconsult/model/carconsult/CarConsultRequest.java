package com.jth.carfinanceconsult.model.carconsult;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarConsultRequest {
    @ApiModelProperty(notes = "차량 옵션 시퀀스", required = true)
    @NotNull
    private Long carOptionId;

    @ApiModelProperty(notes = "고객 이름 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @ApiModelProperty(notes = "고객 전화번호 (12~20자)", required = true)
    @NotNull
    @Length(min = 12, max = 20)
    private String customerPhone;
}
