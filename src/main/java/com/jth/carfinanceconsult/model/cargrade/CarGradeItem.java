package com.jth.carfinanceconsult.model.cargrade;

import com.jth.carfinanceconsult.entity.CarGrade;
import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarGradeItem {
    @ApiModelProperty(notes = "차량 등급 시퀀스")
    private Long carGradeId;
    @ApiModelProperty(notes = "차량 제조사명과 차량명")
    private String carFullName;
    @ApiModelProperty(notes = "차량 모양")
    private String carShape;
    @ApiModelProperty(notes = "차량 출시년도")
    private String carDateRelease;
    @ApiModelProperty(notes = "차량 연료")
    private String carFuel;
    @ApiModelProperty(notes = "차량 배기량")
    private String carDisplacement;

    private CarGradeItem(CarGradeItemBuilder builder) {
        this.carGradeId = builder.carGradeId;
        this.carFullName = builder.carFullName;
        this.carShape = builder.carShape;
        this.carDateRelease = builder.carDateRelease;
        this.carFuel = builder.carFuel;
        this.carDisplacement = builder.carDisplacement;
    }
    public static class CarGradeItemBuilder implements CommonModelBuilder<CarGradeItem> {
        private final Long carGradeId;
        private final String carFullName;
        private final String carShape;
        private final String carDateRelease;
        private final String carFuel;
        private final String carDisplacement;

        public CarGradeItemBuilder(CarGrade carGrade) {
            this.carGradeId = carGrade.getId();
            this.carFullName = "[" + carGrade.getCarModel().getCarManufacturer().getManufacturerName().getName()+ "]" + carGrade.getCarModel().getCarName();
            this.carShape = carGrade.getCarModel().getCarExternal().getName()+ "(" + carGrade.getCarModel().getCarType().getName() + ")";
            this.carDateRelease = carGrade.getCarDateRelease() + "년식";
            this.carFuel = carGrade.getCarFuel().getName();
            this.carDisplacement = carGrade.getCarDisplacement() + "CC";
        }

        @Override
        public CarGradeItem build() {
            return new CarGradeItem(this);
        }
    }
}
