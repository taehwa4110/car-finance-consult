package com.jth.carfinanceconsult.model.cargrade;

import com.jth.carfinanceconsult.enums.CarFuel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarGradeRequest {
    @ApiModelProperty(notes = "차량 모델 시퀀스", required = true)
    @NotNull
    private Long carModelId;

    @ApiModelProperty(notes = "차량 출시년도", required = true)
    @NotNull
    private Integer carDateRelease;

    @ApiModelProperty(notes = "차량 연료", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CarFuel carFuel;

    @ApiModelProperty(notes = "차량 배기량", required = true)
    @NotNull
    private Integer carDisplacement;
}
