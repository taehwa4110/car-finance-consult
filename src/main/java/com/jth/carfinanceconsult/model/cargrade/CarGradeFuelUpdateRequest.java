package com.jth.carfinanceconsult.model.cargrade;

import com.jth.carfinanceconsult.enums.CarFuel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarGradeFuelUpdateRequest {
    @ApiModelProperty(notes = "차량 연료", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CarFuel carFuel;
}
