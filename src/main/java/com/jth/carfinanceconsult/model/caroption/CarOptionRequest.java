package com.jth.carfinanceconsult.model.caroption;

import com.jth.carfinanceconsult.enums.CarDriveMethod;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
@Getter
@Setter
public class CarOptionRequest {
    @ApiModelProperty(notes = "차량 등급 시퀀스", required = true)
    @NotNull
    private Long carGradeId;

    @ApiModelProperty(notes = "차량 옵션 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String carOptionName;

    @ApiModelProperty(notes = "차량 변속기 (2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String carGearBox;

    @ApiModelProperty(notes = "차량 구동방식", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private CarDriveMethod carDriveMethod;

    @ApiModelProperty(notes = "차량 최고 출력", required = true)
    @NotNull
    private Integer carHighOutput;

    @ApiModelProperty(notes = "차량 최대 토크", required = true)
    @NotNull
    private Double carMaximumTorque;

    @ApiModelProperty(notes = "차량 복합연비", required = true)
    @NotNull
    private Double carCompoundFuelEconomy;

    @ApiModelProperty(notes = "차량 도심연비", required = true)
    @NotNull
    private Double carCityFuelEconomy;

    @ApiModelProperty(notes = "차량 고속도로 연비", required = true)
    @NotNull
    private Double carHighwayFuelEconomy;

    @ApiModelProperty(notes = "차량 연비 등급", required = true)
    @NotNull
    private Integer carFuelGrade;

    @ApiModelProperty(notes = "차량 저공해 등급", required = true)
    @NotNull
    private Integer carLowPollutionGrade;

    @ApiModelProperty(notes = "차량 공차중량", required = true)
    @NotNull
    private Double carCurbWeight;

    @ApiModelProperty(notes = "차량 정원", required = true)
    @NotNull
    private Integer carPersonnel;

    @ApiModelProperty(notes = "차량 가격", required = true)
    @NotNull
    private Double carPrice;
}
