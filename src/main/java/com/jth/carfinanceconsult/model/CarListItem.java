package com.jth.carfinanceconsult.model;

import com.jth.carfinanceconsult.entity.CarModel;
import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarListItem {
    @ApiModelProperty(notes = "차량 모델 시퀀스")
    private Long carModelId;
    @ApiModelProperty(notes = "차량 출시상태")
    private String carReleaseSituationName;
    @ApiModelProperty(notes = "차량 이미지")
    private String carImageUrl;
    @ApiModelProperty(notes = "차량 제조사명과 차량명")
    private String carFullName;

    private CarListItem(CarListItemBuilder builder) {
        this.carModelId = builder.carModelId;
        this.carReleaseSituationName = builder.carReleaseSituationName;
        this.carImageUrl = builder.carImageUrl;
        this.carFullName = builder.carFullName;
    }

    public static class CarListItemBuilder implements CommonModelBuilder<CarListItem> {
        private final Long carModelId;
        private final String carReleaseSituationName;
        private final String carImageUrl;
        private final String carFullName;

        public CarListItemBuilder(CarModel carModel) {
            this.carModelId = carModel.getId();
            this.carReleaseSituationName = carModel.getCarReleaseSituation().getName();
            this.carImageUrl = carModel.getCarImageUrl();
            this.carFullName = "[" + carModel.getCarManufacturer().getManufacturerName().getName() + "]" + carModel.getCarName();
        }

        @Override
        public CarListItem build() {
            return new CarListItem(this);
        }
    }
}
