package com.jth.carfinanceconsult.model.manufacturer;

import com.jth.carfinanceconsult.enums.ManufacturerName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarManufacturerJoinRequest {
    @ApiModelProperty(notes = "차량 제조사 이름", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private ManufacturerName manufacturerName;

    @ApiModelProperty(notes = "차량 제조사 마크이미지 (~100자)", required = true)
    @NotNull
    @Length(max = 100)
    private String manufacturerMarkImage;

    @ApiModelProperty(notes = "차량 제조사 전화번호 (12 ~ 20자)", required = true)
    @NotNull
    @Length(min = 12, max = 20)
    private String manufacturerPhone;
}
