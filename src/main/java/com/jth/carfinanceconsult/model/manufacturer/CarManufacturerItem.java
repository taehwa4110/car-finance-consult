package com.jth.carfinanceconsult.model.manufacturer;

import com.jth.carfinanceconsult.entity.CarManufacturer;
import com.jth.carfinanceconsult.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarManufacturerItem {
    @ApiModelProperty(notes = "차량 제조사 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "차량 제조사 이름")
    private String carManufacturerName;
    @ApiModelProperty(notes = "차량 제조사 마크이미지")
    private String carManufacturerMarkImage;
    @ApiModelProperty(notes = "차량 제조사 번호")
    private String carManufacturerPhone;

    @ApiModelProperty(notes = "차량 제조사 운영 여부")
    private String isOperate;

    @ApiModelProperty(notes = "차량 제조사 시작일")
    private LocalDateTime dateOpen;

    @ApiModelProperty(notes = "차량 제조사 폐사일")
    private LocalDateTime dateClose;

    private CarManufacturerItem(CarManufacturerItemBuilder builder) {
        this.id = builder.id;
        this.carManufacturerName = builder.carManufacturerName;
        this.carManufacturerMarkImage = builder.carManufacturerMarkImage;
        this.carManufacturerPhone = builder.carManufacturerPhone;
        this.isOperate = builder.isOperate;
        this.dateOpen = builder.dateOpen;
        this.dateClose = builder.dateClose;
    }
    public static class CarManufacturerItemBuilder implements CommonModelBuilder<CarManufacturerItem> {
        private final Long id;
        private final String carManufacturerName;
        private final String carManufacturerMarkImage;
        private final String carManufacturerPhone;
        private final String isOperate;
        private final LocalDateTime dateOpen;
        private final LocalDateTime dateClose;


        public CarManufacturerItemBuilder(CarManufacturer carManufacturer) {
            this.id = carManufacturer.getId();
            this.carManufacturerName = carManufacturer.getManufacturerName().getName();
            this.carManufacturerMarkImage = carManufacturer.getManufacturerMarkImage();
            this.carManufacturerPhone = carManufacturer.getManufacturerPhone();
            this.isOperate = carManufacturer.getIsOperate() ? "예" : "아니오";
            this.dateOpen = carManufacturer.getDateOpen();
            this.dateClose = carManufacturer.getDateClose();
        }

        @Override
        public CarManufacturerItem build() {
            return new CarManufacturerItem(this);
        }
    }
}
