## 자동차 금융 상담 관리 프로그램

### LANGUAGE
```
JAVA 16
SpringBoot 2.7.3
```

### 기능
```
* 차량 정보 관리 
 - 차량 제조사 등록
 - 차량 제조사 운영 여부 수정하기
 - 차량 모델 등록하기
 - 차량 타입 수정
 - 차량 등급 등록하기
 - 차량 연료 정보 수정
 - 차량 옵션 등록하기
 - 차량 옵션 정보 수정하기
 - 차량 리스트 가져오기
 - 차량 상세정보 가져오기
 
* 자동차 금융 상담 관리 
 - 차량 금융 상담 내역 등록하기
 - 기간 안에 등록된 차량 금융 상담 내역 리스트 가져오기
```

### 예시
>* 스웨거 전체 화면
>
> ![swagger-all](./images/swagger-all.png)

>* 스웨거 차량 정보 관리 화면
>
> ![swagger-car-info](./images/swagger-car-info.png)

>* 스웨거 차량 금융 상담 관리 화면
>
> ![swagger-finance-consult](./images/swagger-car-finance-consult.png)